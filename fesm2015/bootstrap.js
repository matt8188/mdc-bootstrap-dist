import { __decorate } from 'tslib';
import { ɵɵdefineInjectable, Injectable, Component, NgModule } from '@angular/core';

let BootstrapService = class BootstrapService {
    constructor() { }
};
BootstrapService.ɵprov = ɵɵdefineInjectable({ factory: function BootstrapService_Factory() { return new BootstrapService(); }, token: BootstrapService, providedIn: "root" });
BootstrapService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], BootstrapService);

let BootstrapComponent = class BootstrapComponent {
    constructor() { }
    ngOnInit() {
    }
};
BootstrapComponent = __decorate([
    Component({
        selector: 'mdc-bootstrap-bootstrap',
        template: `
    <p>
      bootstrap works!
    </p>
  `
    })
], BootstrapComponent);

let BootstrapModule = class BootstrapModule {
};
BootstrapModule = __decorate([
    NgModule({
        declarations: [BootstrapComponent],
        imports: [],
        exports: [BootstrapComponent]
    })
], BootstrapModule);

/*
 * Public API Surface of bootstrap
 */

/**
 * Generated bundle index. Do not edit.
 */

export { BootstrapComponent, BootstrapModule, BootstrapService };
//# sourceMappingURL=bootstrap.js.map
