import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { BootstrapComponent } from './bootstrap.component';
var BootstrapModule = /** @class */ (function () {
    function BootstrapModule() {
    }
    BootstrapModule = __decorate([
        NgModule({
            declarations: [BootstrapComponent],
            imports: [],
            exports: [BootstrapComponent]
        })
    ], BootstrapModule);
    return BootstrapModule;
}());
export { BootstrapModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0cmFwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdHJhcC8iLCJzb3VyY2VzIjpbImxpYi9ib290c3RyYXAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBVTNEO0lBQUE7SUFBK0IsQ0FBQztJQUFuQixlQUFlO1FBTjNCLFFBQVEsQ0FBQztZQUNSLFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO1lBQ2xDLE9BQU8sRUFBRSxFQUNSO1lBQ0QsT0FBTyxFQUFFLENBQUMsa0JBQWtCLENBQUM7U0FDOUIsQ0FBQztPQUNXLGVBQWUsQ0FBSTtJQUFELHNCQUFDO0NBQUEsQUFBaEMsSUFBZ0M7U0FBbkIsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCb290c3RyYXBDb21wb25lbnQgfSBmcm9tICcuL2Jvb3RzdHJhcC5jb21wb25lbnQnO1xuXG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQm9vdHN0cmFwQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbQm9vdHN0cmFwQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBCb290c3RyYXBNb2R1bGUgeyB9XG4iXX0=