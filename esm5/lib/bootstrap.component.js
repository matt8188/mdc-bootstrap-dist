import { __decorate } from "tslib";
import { Component } from '@angular/core';
var BootstrapComponent = /** @class */ (function () {
    function BootstrapComponent() {
    }
    BootstrapComponent.prototype.ngOnInit = function () {
    };
    BootstrapComponent = __decorate([
        Component({
            selector: 'mdc-bootstrap-bootstrap',
            template: "\n    <p>\n      bootstrap works!\n    </p>\n  "
        })
    ], BootstrapComponent);
    return BootstrapComponent;
}());
export { BootstrapComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0cmFwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdHJhcC8iLCJzb3VyY2VzIjpbImxpYi9ib290c3RyYXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBWWxEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQixxQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUxVLGtCQUFrQjtRQVY5QixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLFFBQVEsRUFBRSxpREFJVDtTQUdGLENBQUM7T0FDVyxrQkFBa0IsQ0FPOUI7SUFBRCx5QkFBQztDQUFBLEFBUEQsSUFPQztTQVBZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ21kYy1ib290c3RyYXAtYm9vdHN0cmFwJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8cD5cbiAgICAgIGJvb3RzdHJhcCB3b3JrcyFcbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlczogW1xuICBdXG59KVxuZXhwb3J0IGNsYXNzIEJvb3RzdHJhcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iXX0=