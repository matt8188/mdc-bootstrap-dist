import { __decorate } from 'tslib';
import { ɵɵdefineInjectable, Injectable, Component, NgModule } from '@angular/core';

var BootstrapService = /** @class */ (function () {
    function BootstrapService() {
    }
    BootstrapService.ɵprov = ɵɵdefineInjectable({ factory: function BootstrapService_Factory() { return new BootstrapService(); }, token: BootstrapService, providedIn: "root" });
    BootstrapService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], BootstrapService);
    return BootstrapService;
}());

var BootstrapComponent = /** @class */ (function () {
    function BootstrapComponent() {
    }
    BootstrapComponent.prototype.ngOnInit = function () {
    };
    BootstrapComponent = __decorate([
        Component({
            selector: 'mdc-bootstrap-bootstrap',
            template: "\n    <p>\n      bootstrap works!\n    </p>\n  "
        })
    ], BootstrapComponent);
    return BootstrapComponent;
}());

var BootstrapModule = /** @class */ (function () {
    function BootstrapModule() {
    }
    BootstrapModule = __decorate([
        NgModule({
            declarations: [BootstrapComponent],
            imports: [],
            exports: [BootstrapComponent]
        })
    ], BootstrapModule);
    return BootstrapModule;
}());

/*
 * Public API Surface of bootstrap
 */

/**
 * Generated bundle index. Do not edit.
 */

export { BootstrapComponent, BootstrapModule, BootstrapService };
//# sourceMappingURL=bootstrap.js.map
